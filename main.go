package main

import (
	"bufio"
	"fmt"
	"log"
	"net"
	"os"
	"os/exec"
	"strings"
	
	c "github.com/logrusorgru/aurora"
)

const SHELL = "\nnc:#> "

func cleanResponse(buf []byte) string {
	var resp []byte

	for _, v := range buf {
		if v == '0' {
			return string(resp)
		}
		
		resp = append(resp, v)
	}

	return string(resp)
}

func RunClient(host string, port int) error {
	conn, err := net.Dial("tcp", fmt.Sprintf("%v:%v", host, port))
	if err != nil {
		return err
	}

	for {
		recvn := 1
		var response []byte

		for recvn > 0 {
			buf := make([]byte, 4096)
			
			recvn, err := conn.Read(buf)
			if err != nil {
				log.Printf("failed to read from connection: %v", err)
				return err
			}

			for _, b := range buf {
				response = append(response, b)
			}

			if recvn < 4096 {
				break
			}
		}
		
		resp := cleanResponse(response)
		if resp == SHELL {
			fmt.Print(c.Bold(c.Yellow(resp)))
		} else {
			fmt.Print(string(resp))
		}

		payload, err := bufio.NewReader(os.Stdin).ReadString('\n')
		if err != nil {
			log.Println("erro read full2: ", err)
			return err
		}

		_, err = conn.Write([]byte(payload))
		if err != nil {		
			log.Printf("failed to send payload: %v", err)
			return err
		}
	}

	conn.Close()
	return nil
}

func Listen(host string, port int) error {
	ln, err := net.Listen("tcp", fmt.Sprintf("%v:%v", host, port))
	if err != nil {
		return err
	}

	for {
		conn, err := ln.Accept()
		if err != nil {
			return err
		}

		go handleConnection(conn)
	}
}

func run(cmd string) []byte {
	cmd = strings.Trim(cmd, "\n\r\x00")
	split := strings.Split(cmd, " ")

	var err error
	var out []byte
	
	if len(split) == 1 {
		out, err = exec.Command(split[0]).Output()
	} else {
		log.Println(split)
		out, err = exec.Command(split[0], split[1:len(split)-1]...).Output()
	}
	
	if err != nil {
		return []byte(fmt.Sprintf("failed to execute command: %v", err))
	}

	return out
}

func handleConnection(conn net.Conn) error {
	for {
		if _, err := conn.Write([]byte(SHELL)); err != nil {
			log.Println("failed to write shell header: ", err)
			return err
		}

		cmd := make([]byte, 4096)
		_, err := conn.Read(cmd)
		if err != nil {
			log.Printf("failed to read from connection: ", err)
			return err
		}

		if _, err := conn.Write(run(string(cmd))); err != nil {
			log.Printf("failed to write response: %v", err)
		}
		
	}
}

func main() {
	go func() {
		log.Fatal(Listen("localhost", 8090))
	}()


	RunClient("localhost", 8090)
}
